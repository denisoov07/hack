import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueResource from 'vue-resource'
import Vuetify from 'vuetify/lib'


Vue.use(Vuetify)
Vue.use(VueResource)
Vue.config.productionTip = false
const opts = {}

export default new Vuetify(opts)
new Vue({

  Vuetify,
  VueResource,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
