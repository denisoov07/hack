import Vue from 'vue'
import Router from 'vue-router'
import Posts from '../views/Posts.vue'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Registration from '../views/Registration.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/posts',
      name: 'posts',
      component: Posts
    },
    {
      path: '',
      name: 'Home',
      component: Home
    },
    {
      path: '/Registration',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/About',
      name: 'About',
      component: About
    }
  ]
})

